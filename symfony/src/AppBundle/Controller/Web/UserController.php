<?php
namespace AppBundle\Controller\Web;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;


class UserController extends Controller{


  /**
   *
   * @Route("/new-user", name="web_user_new")
   *
   */

  public function newAction(Request $request)
  {
    return $this->render('default/new.html.twig', []);
  }


  /**
   *
   * @Route("/list-users", name="web_user_list")
   *
   */

  public function listAction(){

    $response = $this->forward('AppBundle:Api/UserApi:list');

    $users = json_decode($response->getContent())->users;

    return $this->render('default/list.html.twig', [
      'users' => $users,
    ]);
  }

  /**
   *
   * @Route("/detail-user/{id}", name="web_user_detail")
   *
   */

  public function detailAction($id) {


    $response = $this->forward('AppBundle:Api/UserApi:show', ['id' => $id]);

    if ($response->getStatusCode() !== 200) {
      $msg = sprintf('Sorry user with id %d does not exist', $id);
      throw $this->createNotFoundException($msg);
    }

    $user =$response->getContent();

    return $this->render('default/detail.html.twig', [
        'user' => json_decode($user),
      ]);


  }

}