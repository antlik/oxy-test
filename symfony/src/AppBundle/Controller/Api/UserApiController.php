<?php
/**
 * Created by PhpStorm.
 * User: janantl
 * Date: 17/07/2019
 * Time: 16:03
 */

namespace AppBundle\Controller\Api;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use FOS\RestBundle\Controller\Annotations as Rest;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation\Model;

use AppBundle\Entity\User ;

class UserApiController extends FOSRestController {


  /**
   * @Rest\Post("/api/users/")
   *
   * @param \http\Env\Request
   *
   * Create new user
   *
   * Endpoint for creating new user
   *
   * @SWG\Response(
   *     response=201,
   *     description="Endpoint for creating a new user",
   *     @SWG\Schema(
   *         type="array",
   *         @SWG\Items(ref=@Model(type=AppBundle\Entity\User::class, groups={"full"}))
   *     )
   * )

   * @SWG\Parameter(
   *     name="Request",
   *     in="query",
   *     type="string",
   *     description="HTTP request, user in JSON format expected"
   * )
   * @SWG\Tag(name="user")
   */
  public function newAction(Request $request){

    $data = json_decode($request->getContent(), true);

    //@TODO test data structure

    $user = new User();
    $user->setName($data['name']);
    $user->setEmail($data['email']);
    $user->setPermission($data['permission']);
    $user->setPassword(password_hash($data['password'],PASSWORD_BCRYPT));

    $em = $this->getDoctrine()->getManager();
    $em->persist($user);
    $em->flush();

    $data = $this->serializeUser($user);
    $response = new Response(json_encode($data), 201);
    $response->headers->set('Content-Type', 'application/json');

    return $response;
  }

  /**
   *
   * List users
   *
   * Endpoint for getting users from database
   *
   * @Rest\Get("/api/users/", name="api_users_show")
   *
   *
   * @SWG\Response(
   *     response=200,
   *     description="Returning JSON of users",
   *     @SWG\Schema(
   *         type="array",
   *         @SWG\Items(ref=@Model(type=User::class, groups={"full"}))
   *     )
   * )
   * @SWG\Response(
   *     response=404,
   *     description="No users found",
   * )
   * @SWG\Tag(name="user")
   */

  public function listAction()
  {
    $users = $this->getDoctrine()
      ->getRepository('AppBundle:User')
      ->findAll();
    $data = array('users' => array());

    foreach ($users as $user) {
      $data['users'][] = $this->serializeUser($user);
    }

    if (empty($users)){
      $response = new Response(json_encode($data), 404);
      $response->headers->set('Content-Type', 'application/json');
      return $response;
    }
    $response = new Response(json_encode($data), 200);
    $response->headers->set('Content-Type', 'application/json');
    return $response;
  }

  /**
   *
   * Get user details
   *
   * Getting user detail form database by ID
   *
   *
   * @Rest\Get("/api/users/{id}", name="api_user_show")
   *
   * @SWG\Response(
   *     response=200,
   *     description="User found and returning data",
   *     @SWG\Schema(
   *         type="array",
   *         @SWG\Items(ref=@Model(type=User::class, groups={"full"}))
   *     )
   * )
   *
   * @SWG\Response(
   *     response=404,
   *     description="User not found",
   *     @SWG\Schema(
   *         type="array",
   *         @SWG\Items(ref=@Model(type=User::class, groups={"full"}))
   *     )
   * )
   * @SWG\Tag(name="user")
   */

  public function showAction($id)
  {

    $user = $this->getDoctrine()
      ->getRepository('AppBundle:User')
      ->findOneById($id);

      if (!$user) {
        throw $this->createNotFoundException(sprintf(
          'No user found with id "%d"',
          $id
        ));
      }

      $data = $this->serializeUser($user);

      $response = new Response(json_encode($data), 200);
      $response->headers->set('Content-Type', 'application/json');
      return $response;

  }

  private function serializeUser(User $user)
  {
    return array(
      'id' => $user->getId(),
      'name' => $user->getName(),
      'email' => $user->getEmail(),
      'permission' => $user->getPermission(),
    );
  }
}