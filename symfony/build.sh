#!/usr/bin/env bash
cp ./app/config/parameters.yml ./app/config/parameters.yml.dist

echo "CREATE DIRS"

mkdir ./var/sessions
mkdir ./var/logs
mkdir ./var/cache

chmod a+w -R ./var/sessions
chmod a+w -R ./var/cache
chmod a+w -R ./var/logs
COMPOSER_MEMORY_LIMIT=-1 composer install
echo "result of composer install"
echo $$

chmod a+w -R ./var/sessions
chmod a+w -R ./var/cache
chmod a+w -R ./var/logs

php bin/console assets:install --symlink
