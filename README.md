#User management

1. RUN: `docker-compose build`
2. RUN: `docker-compose up`
3. for DB sync RUN: `./migrate-db.sh`
  - hint: will work only if you clone to the same directory name as the bitbucket repo (oxy-test),
  if you want to use different directory name please change the name in first grep and add suffix \_php
4. Update your system host file (add symfony.localhost) -> `http://symfony.localhost`


##Used stack:
PHP, Symfony3, MySQL, nginx, Docker

###Docker file:
https://github.com/maxpou/docker-symfony

###Symfony bundles:
####FOSRestBundle
https://github.com/FriendsOfSymfony/FOSRestBundle
- setting up REST API
####NelmioCorsBundle
https://github.com/nelmio/NelmioCorsBundle
- setting permissions for API calls
####JMSSerializerBundle
https://jmsyst.com/bundles/JMSSerializerBundle
- serializing JSON objects
####NelmioApiDocBundle
https://github.com/nelmio/NelmioApiDocBundle
- genetaing API docs
